module.exports = function(grunt) {
  'use strict';

  grunt.loadNpmTasks('@benningfield-group/grunt-build-angularjs');

  grunt.initConfig({
    buildAngularJS: {
      prod: {
        options: {
          // The name of your base AngularJS module.  There must be a JS file
          // in the root directory matching this name (e.g.
          // BGI.GruntBuildAngularJSExample.js).
          angularModuleName: 'BGI.GruntBuildAngularJSExample',

          // The index file for your single-page app.  This is optional and
          // defaults to index.html.
          indexFile: 'index.html'
        }
      }
    }
  });

  grunt.registerTask('default', [
    'buildAngularJS'
  ]);
};

