(function(angular) {
  'use strict';

  angular.module('BGI.GruntBuildAngularJSExample')
    .component('clock', {
      templateUrl  : 'clock/clock.html',
      controller   : ['$window', '$timeout', ClockCtrl],
      controllerAs : 'vm'
    });

  function ClockCtrl($window, $timeout) {
    const vm = this;

    const easel = $window.document.getElementById('easel');
    const ctx   = easel.getContext('2d');

    vm.time = new Date();

    $window.requestAnimationFrame(updateTime);

    function updateTime() {
      vm.time = new Date();

      ctx.clearRect(0, 0, easel.width, easel.height);

      // Milliseconds.
      ctx.beginPath();
      ctx.arc(50, 50, 5, 0, vm.time.getMilliseconds() / 1000 * Math.PI * 2);
      ctx.strokeStyle = '#642718';
      ctx.lineWidth = 10;
      ctx.stroke();

      // Seconds.
      ctx.beginPath();
      ctx.arc(50, 50, 15, 0, vm.time.getSeconds() / 60 * Math.PI * 2);
      ctx.strokeStyle = '#F4789C';
      ctx.lineWidth = 10;
      ctx.stroke();

      // Minutes.
      ctx.beginPath();
      ctx.arc(50, 50, 25, 0, vm.time.getMinutes() / 60 * Math.PI * 2);
      ctx.strokeStyle = '#379812';
      ctx.lineWidth = 10;
      ctx.stroke();

      // Hours.
      ctx.beginPath();
      ctx.arc(50, 50, 35, 0, vm.time.getHours() / 24 * Math.PI * 2);
      ctx.strokeStyle = '#817FF2';
      ctx.lineWidth = 10;
      ctx.stroke();

      $timeout(() => $window.requestAnimationFrame(updateTime), 100);
    }
  }
})(window.angular);

